# Gestion d'un tournoi amical de badminton

Ce programme permet d'aider à l'organisation d'un tournoi de badminton en mode amical

Le programme implemente les contraintes suivantes :

- un joueur ne doit pas se retrouver à devoir jouer sur deux terrains dans
  la même série
- chaque joueur doit avoir effectué à peu près le même nombre de matchs que
  les autres à la fin du tournoi
- chaque joueur change de partenaire à chaque match
- chaque joueur change d'adversaires à chaque match (dans la mesure du possible)

L'organisateur du tournoi indique le nombre de joueurs, le nombre de terrains
et le nombre de series qu'il souhaite

![Ecran principal](images/badminton.png)

## EXECUTER LE PROGRAMME

aller dans le repertoire Tournoi

si ce n'est pas déjà fait, installer le logicile "venv"

```bash
sudo  apt-get install python3-venv

```

créer un environment virtuel python que l'on appelera "tournoi"
(vous pouvez choisir un autre nom) pour notre programme
(à ne faire qu'une seule fois)

```bash
python3 -m venv tournoi

```

activer l'environnement virtual python

```bash
source tournoi/bin/activate
```

installer les library python nécessaires

```bash
pip3 install -r requirements.txt
```

lancer le programme tournoi.py

```bash
python3 tournoi.py
```

Dans votre terminal, vous devriez voir :

![Ecran principal](images/ecran0.png)

## UTILISER LE PROGRAMME

L'organisateur doit modifier le fichier contenant le nom des joueurs

aller dans le répertoire "players"

S'il s'agit d'un tournoi simple, éditer le fichier "player_names.yaml"

S'il s'agit d'un double tournoi en alternance, éditer les fichiers
"player_names_t1.yaml" et "player_names_t2.yaml"

Le répertoire "archive_tournois" contient vos anciens tournois
avec les listes des joueurs (cela peut servir comme liste de base).

Le fichier est un fichier YAML : il faut respecter l'organisation
et la syntaxe de ce fichier. Ne changer que les noms des joueurs.

Lancer un navigateur Internet : firefox ou Edge ou Chrome...

Entrez l'adresse suivante :

```bash
http://localhost:5000
```

vous devriez arriver sur la page suivante

![Ecran options](images/ecran1.png)

Après validation des options, la liste des joueurs est présentée
(il est encore possible de changer des noms)

![Ecran joueurs](images/ecran1.png)

Après validation, la liste des joueurs et le nombre de match est présenté.
Il est possible de ré_organisé les joueurs en fonctions du nombre de matchs.
C'est utile losque tout le monde ne fera pas le meme nombre de matchs dans la soirée.

![Ecran joueurs](images/ecran3.png)

Après validation, l'écran principal apparait :

![Ecran joueurs](images/ecran4.png)

L'organisateur sélectionnera le menu "series" et la serie qu'il souhaite lancer :

L'écran de la série apparait :

![Ecran joueurs](images/ecran4.png)

Tous les joueurs peuvent alors savoir avec qui ils jouent.
L'organisateur peut lancer la serie.
Un minuteur est disponible dans la menu "Minuteur".
Une nouvelle page dans un  nouvel onglet apparait.

Quand la série est terminée, on revient sur la page de la série
et on saisie les scores.

Le menu "Resultats" permet de consulter les saisies des séries.

Le menu "Classement" permet de consulter le classement.
Celui-ci est calculé en fonction du nombre de match gagnés
et des points marqués/encaissés.

Comme il y a 2 groupes de joueurs (les "confirmés" et les "moins confirmés",
il y aura un classement pour chacun de ces deux groupes.

![Ecran joueurs](images/ecran6.png)
