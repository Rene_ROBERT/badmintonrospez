#!/usr/bin/env python3
# coding: utf-8
"""
    fonctions
"""

from operator import itemgetter
import yaml
from flask import request


def calcul_players(group):
    """
    fonction de preparation des données concernant les joueurs d'un groupe
    """
    output_players = []
    level_one = group['players']['levelOne']
    level_two = group['players']['levelTwo']
    for player in level_one:
        player['category'] = 'levelOne'
        player['group'] = group['name']
        player['nbWins'] = 0
        player['nbLooses'] = 0
        player['totalPointMarques'] = 0
        player['totalPointEncaisses'] = 0
        player['classement'] = 0
        player['diffPoints'] = 0
        player['nbJoues'] = 0
        output_players.append(player)
    for player in level_two:
        player['category'] = 'levelTwo'
        player['group'] = group['name']
        player['nbWins'] = 0
        player['nbLooses'] = 0
        player['totalPointMarques'] = 0
        player['totalPointEncaisses'] = 0
        player['classement'] = 0
        player['diffPoints'] = 0
        player['nbJoues'] = 0
        output_players.append(player)
    # on produit en sortie la liste des joueurs du group
    return output_players


def update_players_nbmatch(players_all, player_occurence_in_series):
    """
    fonction de mise à jour des données
    """
    list_nbmatch = []
    for player1 in players_all:
        for player2 in player_occurence_in_series:
            if player1['name'] == player2:
                player1['nbmatch'] = player_occurence_in_series[player2]
        list_nbmatch.append(player1['nbmatch'])
    return list_nbmatch


def read_players(directory, filename):
    """
        read player list from file
    """
    with open(directory + '/' + filename) as yaml_file:
        player_list = yaml.safe_load(yaml_file)
    return player_list


def normalized_names(l1_players, l2_players):
    """
        normalisation des noms pour detecter les doublons
    """
    ck_l1_players = []
    for name in l1_players:
        name = name.lower()
        name = name.replace('é', 'e')
        name = name.replace('è', 'e')
        name = name.replace('ç', 'c')
        name = name.replace('-', ' ')
        name = name.capitalize()
        ck_l1_players.append(name)
    ck_l2_players = []
    for name in l2_players:
        name = name.lower()
        name = name.replace('é', 'e')
        name = name.replace('è', 'e')
        name = name.replace('ç', 'c')
        name = name.replace('-', ' ')
        name = name.capitalize()
        ck_l2_players.append(name)
    p_all = ck_l1_players + ck_l2_players
    return p_all


def record_names(l1_players, l2_players, directory, players_file):
    """
        enregistrement des joueurs dans un fichier
    """
    with open(directory + '/' + players_file, 'w') as outfile:
        player_list = {}
        player_list['levelOne'] = l1_players
        player_list['levelTwo'] = l2_players
        yaml.dump(
            player_list,
            outfile,
            default_flow_style=False,
            allow_unicode=True)
    outfile.close()


def handle_names_from_form(l1_players,
                           l2_players,
                           players_file,
                           tournament_style,
                           tournament_id):
    """ gestion des noms venant du formulaire
    """
    # on donne un numéro si le nom est vide
    for idx, player in enumerate(l1_players):
        if player == '':
            player = idx
    for idx, player in enumerate(l2_players):
        if player == '':
            player = idx
    # si jamais il n'y avait pas assez de joueurs dans l'un des
    # groupes, on ajoute un volontaire.
    # si jamais il n'y avait pas assez de joueurs pour obtenir
    # un multiple de 4, on ajoute plusieurs autres volontaires.
    check_not_enough_players(l1_players, l2_players)

    # enregistrement des saisies pour repartir avec les mêmes bases
    # en cas de plantage ou de nouveau tournoi
    record_names(l1_players, l2_players, 'players', players_file)

    # liste de noms normalisés
    p_all = normalized_names(l1_players, l2_players)
    # vérification des doublons dans les noms
    # en cas de doublon détecté, on affiche une page pour
    # permettre de modifier les joueurs
    if len(p_all) != len(set(p_all)):
        return 'problem_name', tournament_id
    if tournament_style == '1':
        return 'players_one_club', "unique"
    if tournament_style == '3' and tournament_id == "first":
        tournament_id = "second"
        return 'players', "second"
    if tournament_style == '3' and tournament_id == "second":
        return 'players_alternat1_club', "second"


def check_not_enough_players(l1_players, l2_players):
    """
        add some volonteers if necessary
    """
    indice = 1
    while len(l1_players) != len(l2_players):
        if len(l1_players) > len(l2_players):
            l2_players.append('l2_volontaire'+str(indice))
        else:
            l1_players.append('l1_volontaire'+str(indice))
        indice = indice + 1
    nb_players = len(l1_players)+len(l2_players)
    if nb_players % 4 != 0:
        l1_players.append('l1_volontaire'+str(indice))
        l2_players.append('l2_volontaire'+str(indice+1))
    return l1_players, l2_players


def select_pre_calc_file(nb_players, nb_fields):
    ''' selection de la serie precalculée
    '''
    with open('precalc/' +
              str(nb_players) +
              '_players_named_' +
              str(nb_fields) +
              '_fields.yaml') as yaml_file:
        player_list = yaml.safe_load(yaml_file)
    yaml_file.close()
    return player_list


def calculate_nb_match_per_player(players):
    ''' calcul du nombre de match par joueur
    '''
    return players


def handling_player_data(level_one_players,
                         level_two_players,
                         nb_fields,
                         file_extention):
    ''' on recupere les données du formulaire, on complete les données
        et on sauvegarde dans un fichier
    '''
    cursor = 1
    lvl_1_players = []
    lvl_2_players = []
    nb_players = len(level_one_players) + len(level_two_players)
    print("level_one_players = ", level_one_players)
    while cursor <= len(level_one_players):
        l1_player = {}
        l2_player = {}
        l1_player_name = request.form['l1_name'+str(cursor)]
        l1_player_alias = request.form['l1_alias'+str(cursor)]
        l2_player_name = request.form['l2_name'+str(cursor)]
        l2_player_alias = request.form['l2_alias'+str(cursor)]
        if l1_player_name != "":
            l1_player['name'] = l1_player_name
            l1_player['alias'] = l1_player_alias
            for player in level_one_players:
                if player['alias'] == l1_player_alias:
                    l1_player['nb_match'] = player['nb_match']
            lvl_1_players.append(l1_player)
            l1_player = {}
        if l2_player_name != "":
            l2_player['name'] = l2_player_name
            l2_player['alias'] = l2_player_alias
            for player in level_two_players:
                if player['alias'] == l2_player_alias:
                    l2_player['nb_match'] = player['nb_match']
            lvl_2_players.append(l2_player)
            l2_player = {}
        if l1_player_name == "":
            l1_player['name'] = l1_player_alias
            l1_player['alias'] = l1_player_alias
            for player in level_one_players:
                if player['alias'] == l1_player_alias:
                    l1_player['nb_match'] = player['nb_match']
            lvl_1_players.append(l1_player)
            l1_player = {}
        if l2_player_name == "":
            l2_player['name'] = l2_player_alias
            l2_player['alias'] = l2_player_alias
            for player in level_two_players:
                if player['alias'] == l2_player_alias:
                    l2_player['nb_match'] = player['nb_match']
            lvl_2_players.append(l2_player)
            l2_player = {}
        cursor = cursor+1
    # si jamais il n'y avait pas assez de joueurs dans l'un des
    # groupes, on ajoute un volontaire.
    if len(lvl_1_players) != len(lvl_2_players):
        if len(lvl_1_players) > len(lvl_2_players):
            lvl_2_players.append('volontaire')
        else:
            lvl_1_players.append('volontaire')

    nb_players = 2*len(lvl_1_players)
    # on enregistre ces saisies pour repartir avec les mêmes bases
    # en cas de plantage ou de nouveau tournoi
    with open('precalc/' +
              str(nb_players) +
              '_players_named_' +
              str(nb_fields) +
              file_extention, 'w') as outfile:
        group_name = 'ROSPEZ'
        new_group = {}
        new_group['group'] = {}
        new_group['group']['name'] = group_name
        new_group['group']['players'] = {}
        new_group['group']['players']['levelOne'] = lvl_1_players
        new_group['group']['players']['levelTwo'] = lvl_2_players
        yaml.dump(
            new_group,
            outfile,
            default_flow_style=False,
            allow_unicode=True)
    outfile.close()
    return nb_players


def calculate_result_series(players_all, series):
    ''' calculate result series
    '''
    # on va contruire la liste RESULT_SERIES
    result_series = []
    result_serie = []
    result_match = {}
    result_match['teamA'] = {}
    result_match['teamA']['idTeam'] = 0
    result_match['teamA']['player1'] = {}
    result_match['teamA']['player2'] = {}
    result_match['teamB'] = {}
    result_match['teamB']['idTeam'] = 0
    result_match['teamB']['player1'] = {}
    result_match['teamB']['player2'] = {}

    for serie_elem in series:
        for match in serie_elem:
            for player in players_all:
                if player['alias'] == match[0]:
                    result_match['teamA']['player1'] = player
                if player['alias'] == match[1]:
                    result_match['teamA']['player2'] = player
                if player['alias'] == match[2]:
                    result_match['teamB']['player1'] = player
                if player['alias'] == match[3]:
                    result_match['teamB']['player2'] = player

            result_match['scoreA'] = 0
            result_match['scoreB'] = 0
            result_serie.append(result_match)
            result_match = {}
            result_match['teamA'] = {}
            result_match['teamB'] = {}
        result_series.append(result_serie)
        result_serie = []
    return result_series


def resultPlayersByLevel_calculation(series_reduced,
                                     result_series,
                                     tournament_style,
                                     min_match,
                                     level_one_players,
                                     level_two_players):
    """
        traitements classement et resultats des joueurs
        par niveau
    """
    for player in level_one_players:
        player['totalPointMarques'] = 0
        player['totalPointEncaisses'] = 0
        player['nbWins'] = 0
        player['nbLooses'] = 0
        player['nbJoues'] = 0
        player['diffPoints'] = 0
        player['rang'] = 0

    for player in level_two_players:
        player['totalPointMarques'] = 0
        player['totalPointEncaisses'] = 0
        player['nbWins'] = 0
        player['nbLooses'] = 0
        player['nbJoues'] = 0
        player['diffPoints'] = 0
        player['rang'] = 0

    for result_serie in result_series:
        num_match_in_that_serie = len(result_serie)
        num_match = 0
        while num_match < num_match_in_that_serie:
            match = result_serie[num_match]
            player1name = match['teamA']['player1']['name']
            player2name = match['teamA']['player2']['name']
            player3name = match['teamB']['player1']['name']
            player4name = match['teamB']['player2']['name']

            for player in level_one_players:
                if player1name == player['name']:
                    if player['nbJoues'] < min_match:
                        pl_m = player['totalPointMarques']
                        pl_e = player['totalPointEncaisses']
                        res_a = result_serie[num_match]['scoreA']
                        res_b = result_serie[num_match]['scoreB']
                        pl_m = pl_m + int(res_a)
                        pl_e = pl_e + int(res_b)
                        player['totalPointMarques'] = pl_m
                        player['totalPointEncaisses'] = pl_e
                        if res_a > res_b:
                            player['nbJoues'] = player['nbJoues']+1
                            player['nbWins'] = player['nbWins']+1
                        elif res_a < res_b:
                            player['nbJoues'] = player['nbJoues']+1
                            player['nbLooses'] = player['nbLooses']+1
                        player['diffPoints'] = pl_m - pl_e
                    else:
                        player['nbJoues'] = player['nbJoues']+1

                if player2name == player['name']:
                    if player['nbJoues'] < min_match:
                        pl_m = player['totalPointMarques']
                        pl_e = player['totalPointEncaisses']
                        res_a = result_serie[num_match]['scoreA']
                        res_b = result_serie[num_match]['scoreB']
                        pl_m = pl_m + int(res_a)
                        pl_e = pl_e + int(res_b)
                        player['totalPointMarques'] = pl_m
                        player['totalPointEncaisses'] = pl_e
                        if res_a > res_b:
                            player['nbJoues'] = player['nbJoues']+1
                            player['nbWins'] = player['nbWins']+1
                        elif res_a < res_b:
                            player['nbLooses'] = player['nbLooses']+1
                            player['nbJoues'] = player['nbJoues']+1
                        player['diffPoints'] = pl_m - pl_e
                    else:
                        player['nbJoues'] = player['nbJoues']+1

                if player3name == player['name']:
                    if player['nbJoues'] < min_match:
                        pl_m = player['totalPointMarques']
                        pl_e = player['totalPointEncaisses']
                        res_a = result_serie[num_match]['scoreA']
                        res_b = result_serie[num_match]['scoreB']
                        pl_m = pl_m+int(res_b)
                        pl_e = pl_e+int(res_a)
                        player['totalPointMarques'] = pl_m
                        player['totalPointEncaisses'] = pl_e
                        if res_a > res_b:
                            player['nbLooses'] = player['nbLooses']+1
                            player['nbJoues'] = player['nbJoues']+1
                        elif res_a < res_b:
                            player['nbWins'] = player['nbWins']+1
                            player['nbJoues'] = player['nbJoues']+1
                        player['diffPoints'] = pl_m - pl_e
                    else:
                        player['nbJoues'] = player['nbJoues']+1

                if player4name == player['name']:
                    if player['nbJoues'] < min_match:
                        pl_m = player['totalPointMarques']
                        pl_e = player['totalPointEncaisses']
                        res_a = result_serie[num_match]['scoreA']
                        res_b = result_serie[num_match]['scoreB']
                        pl_m = pl_m+int(res_b)
                        pl_e = pl_e+int(res_a)
                        player['totalPointMarques'] = pl_m
                        player['totalPointEncaisses'] = pl_e
                        if res_a > res_b:
                            player['nbLooses'] = player['nbLooses']+1
                            player['nbJoues'] = player['nbJoues']+1
                        elif res_a < res_b:
                            player['nbWins'] = player['nbWins']+1
                            player['nbJoues'] = player['nbJoues']+1
                        player['diffPoints'] = pl_m - pl_e
                    else:
                        player['nbJoues'] = player['nbJoues']+1

            for player in level_two_players:
                if player1name == player['name']:
                    if player['nbJoues'] < min_match:
                        pl_m = player['totalPointMarques']
                        pl_e = player['totalPointEncaisses']
                        res_a = result_serie[num_match]['scoreA']
                        res_b = result_serie[num_match]['scoreB']
                        pl_m = pl_m+int(res_a)
                        pl_e = pl_e+int(res_b)
                        player['totalPointMarques'] = pl_m
                        player['totalPointEncaisses'] = pl_e
                        if res_a < res_b:
                            player['nbLooses'] = player['nbLooses']+1
                            player['nbJoues'] = player['nbJoues']+1
                        elif res_a > res_b:
                            player['nbWins'] = player['nbWins']+1
                            player['nbJoues'] = player['nbJoues']+1
                        player['diffPoints'] = pl_m - pl_e
                    else:
                        player['nbJoues'] = player['nbJoues']+1

                if player2name == player['name']:
                    if player['nbJoues'] < min_match:
                        pl_m = player['totalPointMarques']
                        pl_e = player['totalPointEncaisses']
                        res_a = result_serie[num_match]['scoreA']
                        res_b = result_serie[num_match]['scoreB']
                        pl_m = pl_m+int(res_a)
                        pl_e = pl_e+int(res_b)
                        player['totalPointMarques'] = pl_m
                        player['totalPointEncaisses'] = pl_e
                        if res_a < res_b:
                            player['nbLooses'] = player['nbLooses']+1
                            player['nbJoues'] = player['nbJoues']+1
                        elif res_a > res_b:
                            player['nbWins'] = player['nbWins']+1
                            player['nbJoues'] = player['nbJoues']+1
                        player['diffPoints'] = pl_m - pl_e
                    else:
                        player['nbJoues'] = player['nbJoues']+1

                if player3name == player['name']:
                    if player['nbJoues'] < min_match:
                        pl_m = player['totalPointMarques']
                        pl_e = player['totalPointEncaisses']
                        res_a = result_serie[num_match]['scoreA']
                        res_b = result_serie[num_match]['scoreB']
                        pl_m = pl_m+int(res_b)
                        pl_e = pl_e+int(res_a)
                        player['totalPointMarques'] = pl_m
                        player['totalPointEncaisses'] = pl_e
                        if res_a < res_b:
                            player['nbWins'] = player['nbWins']+1
                            player['nbJoues'] = player['nbJoues']+1
                        elif res_a > res_b:
                            player['nbLooses'] = player['nbLooses']+1
                            player['nbJoues'] = player['nbJoues']+1
                        player['diffPoints'] = pl_m - pl_e
                    else:
                        player['nbJoues'] = player['nbJoues']+1

                if player4name == player['name']:
                    if player['nbJoues'] < min_match:
                        pl_m = player['totalPointMarques']
                        pl_e = player['totalPointEncaisses']
                        res_a = result_serie[num_match]['scoreA']
                        res_b = result_serie[num_match]['scoreB']
                        pl_m = pl_m+int(res_b)
                        pl_e = pl_e+int(res_a)
                        player['totalPointMarques'] = pl_m
                        player['totalPointEncaisses'] = pl_e
                        if res_a < res_b:
                            player['nbWins'] = player['nbWins']+1
                            player['nbJoues'] = player['nbJoues']+1
                        elif res_a > res_b:
                            player['nbLooses'] = player['nbLooses']+1
                            player['nbJoues'] = player['nbJoues']+1
                        player['diffPoints'] = pl_m - pl_e
                    else:
                        player['nbJoues'] = player['nbJoues']+1

            num_match = num_match+1
    level_one_players_sorted = sorted(
        level_one_players,
        key=itemgetter('nbWins', 'diffPoints'),
        reverse=True)
    level_two_players_sorted = sorted(
        level_two_players,
        key=itemgetter('nbWins', 'diffPoints'),
        reverse=True)
    return level_one_players_sorted, level_two_players_sorted
