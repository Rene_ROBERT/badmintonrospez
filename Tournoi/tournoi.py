#!/usr/bin/env python3
# coding: utf-8
"""
    programme de gestion de tournoi de badminton en mode loisir
"""
import os
import json
import datetime
from operator import itemgetter
from random import shuffle
import yaml
from flask import Flask, request, render_template, redirect, url_for, session
from flask_session import Session
from datetime import timedelta
from functions.some_functions import (calcul_players,
                                      read_players,
                                      handle_names_from_form,
                                      select_pre_calc_file,
                                      calculate_nb_match_per_player,
                                      handling_player_data,
                                      calculate_result_series,
                                      resultPlayersByLevel_calculation,
                                      record_names)


print('#####################################')
print("Bienvenue dans le programme Badminton")
print("      http://localhost:5000/")
print('#####################################')

# Serveur Web
MON_SERVEUR_WEB = Flask(__name__)


MON_SERVEUR_WEB.config['SESSION_PERMANENT'] = True
MON_SERVEUR_WEB.config['SESSION_TYPE'] = 'filesystem'
MON_SERVEUR_WEB.config['PERMANENT_SESSION_LIFETIME'] = timedelta(hours=5)

# secret_key pour utiliser Session de Flask
MON_SERVEUR_WEB.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

MON_SERVEUR_WEB.config.from_object(__name__)
Session(MON_SERVEUR_WEB)

@MON_SERVEUR_WEB.route('/', methods=['GET', 'POST'])
def reload_or_new_tournament():
    """
        demande si nouveau tournoi ou reprise
        GET = affichage de la page de choix
        POST = sauvegarde, traitement et affichage du menu suivant
    """
    if request.method == 'GET':
        # on affiche une page web de demande d'infos
        return render_template('reload_or_new.j2')

    # request.method == 'POST':
    # on récupère les infos choisies
    reload_or_new = request.form['reload_or_new']

    # on effectue les traitements et on sélectionne
    # le prochain template de page web
    if reload_or_new == '1':       # tournoi 1 club
        return redirect(url_for('tournament_type'))
    #
    # on recharge les données et on affiche directement le tournoi
    with open('save/SERIES_REDUCED.json', 'r') as infile:
        series_reduced = json.load(infile)
    infile.close()
    with open('save/RESULT_SERIES.json', 'r') as infile:
        result_series = json.load(infile)
    infile.close()
    return render_template(
        'welcome.j2',
        series=series_reduced,
        result_series=result_series,
        tournament_style='1')


@MON_SERVEUR_WEB.route('/tournament_type', methods=['GET', 'POST'])
def tournament_type():
    """
        demande de sélection du type de tournoi
        GET = affichage de la page de choix
        POST = sauvegarde, traitement et affichage du menu suivant
    """
    if request.method == 'GET':
        # on affiche une page web de demande d'infos
        return render_template('tournament_type.j2')

    # request.method == 'POST':
    # on récupère les infos choisies
    tournament_style = request.form['tournamentStyle']
    tournament_name = request.form['tournamentName']
    date = datetime.datetime.now()
    date = date.strftime("%d-%m-%Y_%Hh%Mmn")
    tournament_directory = tournament_name + '_' + date
    os.mkdir('archive_tournois/' + tournament_directory)
    nb_series_wanted = int(request.form['numberOfseriesWewant'])
    nb_fields = int(request.form['nb_fields'])

    #players_file = 'player_names.yaml'
    # on sauvegarde les infos choisies dans la session
    session["tournament_name"] = tournament_name
    session["tournament_directory"] = ('archive_tournois/' +
                                       tournament_directory)
    session["tournament_style"] = tournament_style
    session["nb_series_wanted"] = nb_series_wanted
    session["nb_fields"] = nb_fields
    # session["players_file"] = players_file
    # on effectue les traitements et on sélectionne
    # le prochain template de page web
    if tournament_style == '1':       # tournoi 1 club
        session["players_file"] = 'player_names.yaml'
        session["tournament_id"] = 'unique'
        return redirect(url_for('players'))
    if tournament_style == '2':    # tournoi 2 clubs
        return redirect(url_for('players_two_club'))
    # sinon c'est un double tournoi
    session["players_file"] = 'player_names_t1.yaml'
    session["tournament_id"] = 'first'
    return redirect(url_for('players'))


@MON_SERVEUR_WEB.route('/players', methods=['GET', 'POST'])
def players():
    """
        saisies des joueurs
        GET = affichage de la page de saisie
        POST = sauvegarde, traitement et affichage du menu suivant
    """
    # on recupere les noms dans les fichiers du repertoire players
    tournament_style = session.get("tournament_style", None)
    tournament_id = session.get("tournament_id", None)
    if tournament_style == '1':
        player_list = read_players('players', 'player_names.yaml')
    if tournament_style == '3' and tournament_id == "first":
        player_list = read_players('players', 'player_names_t1.yaml')
    if tournament_style == '3' and tournament_id == "second":
        player_list = read_players('players', 'player_names_t2.yaml')
    # on récupère les deux listes de joueurs
    l1_players = player_list['levelOne']
    l2_players = player_list['levelTwo']
    if request.method == 'GET':
        # on affiche une page web afin de permettre la saisie/modif
        # de la liste des joueurs
        doublons = []
        return render_template('players_one_club.j2',
                               players=player_list,
                               doublons=doublons)

    # request.method == 'POST':
    # on récupere les noms à partir du formulaire
    l1_players = request.form.getlist('l1_name')
    l2_players = request.form.getlist('l2_name')
    tournament_id = session.get("tournament_id", None)
    if tournament_style == '1':
        players_file = 'player_names.yaml'
    if tournament_style == '3' and tournament_id == 'first':
        players_file = 'player_names_t1.yaml'
    if tournament_style == '3' and tournament_id == 'second':
        players_file = 'player_names_t2.yaml'
    # effectue quelques verifications et on re-sauvegarde les noms
    next_page, tournament_id = handle_names_from_form(l1_players,
                                                      l2_players,
                                                      players_file,
                                                      tournament_style,
                                                      tournament_id)
    session["tournament_id"] = tournament_id
    return redirect(url_for(next_page))


@MON_SERVEUR_WEB.route('/doublon', methods=['GET', 'POST'])
def problem_name():
    """
        gestion des doublons
        possibilité de changer le nom des joueurs
        GET = affichage de la page de saisie
        POST = sauvegarde, traitement et affichage du menu suivant
    """
    tournament_id = session.get("tournament_id", None)
    players_file = session.get("players_file", None)
    tournament_style = session.get("tournament_style", None)
    if request.method == 'GET':
        # on récupère la liste des joueurs, sauvée précedemment
        player_list = read_players('players', session["players_file"])
        l1_players = player_list['levelOne']
        l2_players = player_list['levelTwo']
        # on identifie les doublons
        p_all = l1_players + l2_players
        doublons = [p_all[i] for i in range(len(p_all)) if i != p_all.index(p_all[i])]
        # on re-affiche une page web afin de permettre la modification
        # de la liste des joueurs
        return render_template('players_one_club.j2',
                               players=player_list,
                               doublons=doublons)

    # request.method == 'POST':
    # on récupere les noms à partir du formulaire
    l1_players = request.form.getlist('l1_name')
    l2_players = request.form.getlist('l2_name')
    # effectue quelques verifications et on re-sauvegarde les noms
    next_page, tournament_id = handle_names_from_form(l1_players,
                                                      l2_players,
                                                      players_file,
                                                      tournament_style,
                                                      tournament_id)
    print(' new tournament_id : ', tournament_id)
    session["tournament_id"] = tournament_id
    return redirect(url_for(next_page))


@MON_SERVEUR_WEB.route('/playersOneClub', methods=['GET', 'POST'])
def players_one_club():
    """
        gestion de la saisie des joueurs pour un tournoi interne
        d'un club
        GET = affichage de la page de saisie des joueurs
        POST = sauvegarde, traitement et affichage du menu suivant
    """

    # on appelle le template d'affichage de liste des joueurs
    if request.method == 'GET':
        # on récupère la liste des noms des joueurs
        player_names = read_players('players', 'player_names.yaml')
        # on mélange les noms afin de mélanger l'ordre d'affichage
        # des joueurs
        shuffle(player_names['levelOne'])
        shuffle(player_names['levelTwo'])

        # on calcule le nombre de joueurs afin de selectionner
        # le fichier de series précalculées
        nb_players = 2*len(player_names['levelOne'])
        nb_fields = session.get("nb_fields", None)
        player_list = select_pre_calc_file(nb_players, nb_fields)
        player_list = calculate_nb_match_per_player(player_list)
        print("nb_players:", nb_players)


        # on récupère les deux listes de joueurs
        level_one_players = player_list['group']['players']['levelOne']
        level_two_players = player_list['group']['players']['levelTwo']

        # on enregistre en session les deux listes
        session["level_one_players"] = level_one_players
        session["level_two_players"] = level_two_players

        # calcul des nombres min et max de matchs prevus pour chaque
        # joueur. Ce sera utilisé pour calculer le classement
        # sur la base des N premiers matchs joués (N = min_match)
        max_match = 0
        min_match = session.get("nb_series_wanted", None)
        
        for elem in level_one_players+level_two_players:
            if elem['nb_match'] > max_match:
                max_match = elem['nb_match']
            if elem['nb_match'] < min_match:
                min_match = elem['nb_match']

        # on enregistre en session le nombre de match pour les calculs
        # de classement
        session["min_match"] = min_match
        print("nb_series_wanted: ", session.get("nb_series_wanted", None))
        print("Max_Match :", max_match)

        print("Min_Match :", min_match)

        # on selectionne le prochain template de page web
        # le template est différent en fonction de la relation entre
        # le nombre de joueurs et le nombre de terrain : si on a
        # suffisamment de terrain, tous les joueurs feront
        # le meme nombre de match. Sinon, l'organisateur doit décider
        # quel joueur fera plus ou moins de match que les autres
        if len(level_one_players) <= nb_fields*2:
            return render_template(
                'players_one_club_less_nbfields.j2',
                level_one_players=level_one_players,
                level_two_players=level_two_players,
                group_name=player_list['group']['name'],
                player_names=player_names)
        return render_template(
            'players_one_club_more_nbfields.j2',
            level_one_players=level_one_players,
            level_two_players=level_two_players,
            group_name=player_list['group']['name'],
            player_names=player_names)

    # request.method == 'POST':

    # on récupère le nom de chaque joueur qui aura été associé à
    # l'alias qui a servi pour le calcul des séries et du nombre
    # de matchs prévu
    level_one_players = session.get("level_one_players", None)
    level_two_players = session.get("level_two_players", None)
    nb_fields = session.get("nb_fields", None)

    nb_players = handling_player_data(level_one_players,
                                      level_two_players,
                                      nb_fields,
                                      '_fields.yaml')
    session["nb_players"] = nb_players
    tournament_style = session.get("tournament_style", None)
    fichier = open('precalc/' +
                   str(nb_players) +
                   '_players_named_' +
                   str(nb_fields) +
                   '_fields.yaml')
    data_map = yaml.safe_load(fichier)
    fichier.close()
    # creation des listes
    level_one_players = data_map['group']['players']['levelOne']
    level_two_players = data_map['group']['players']['levelTwo']
    # nb_players_level = len(level_one_players)

    # la fonction calcul_players permet de créer
    # une liste de joueurs avec leurs caractéristiques
    players_all = calcul_players(data_map['group'])
    session["players_all"] = players_all

    for index, player in enumerate(players_all):
        player['id'] = index+1

    # on ouvre le fichier des series pre-calculées par
    # le programme test
    nb_series_wanted = session.get("nb_series_wanted", None)
    number_of_series = nb_series_wanted
    if number_of_series not in (7, 10, 12, 14):
        number_of_series = 10
    session["number_of_series"] = number_of_series
    with open('precalc/' +
              str(nb_players) +
              '_players_' +
              str(number_of_series) +
              '_series_' +
              str(nb_fields) +
              '_fields.json') as json_data:
        series = json.load(json_data)
    json_data.close()

    result_series = calculate_result_series(players_all, series)
    series_reduced = list(result_series)
    session["result_series"] = result_series
    session["series_reduced"] = series_reduced

    with open('series/series_' +
              str(nb_players) +
              '_players.json', 'w') as outfile:
        json.dump(series, outfile, indent=4)
    outfile.close()
    session["level_one_players"] = level_one_players
    session["level_two_players"] = level_two_players
    return render_template(
        'welcome.j2',
        series=series,
        result_series=result_series,
        tournament_style=tournament_style)


@MON_SERVEUR_WEB.route('/playersAlternat1Club', methods=['GET', 'POST'])
def players_alternat1_club():
    """
        gestion de la saisie des joueurs pour un double tournoi
        en alternance. Bien adapté quand on a beaucoup de joueurs et
        peu de terrains
        GET = affichage des pages de saisie des joueurs
        POST = sauvegarde, traitement et affichage du menu suivant
    """

    # on appelle le template d'affichage de liste des joueurs
    if request.method == 'GET':
        # on récupère la liste des joueurs du tournoi 1
        player_names_t1 = read_players('players', 'player_names_t1.yaml')
        # on mélange les noms afin de mélanger l'ordre d'affichage
        # des joueurs
        shuffle(player_names_t1['levelOne'])
        shuffle(player_names_t1['levelTwo'])
        # on calcule le nombre de joueurs afin de selectionner
        # le fichier de series précalculées
        nb_players_t1 = 2*len(player_names_t1['levelOne'])
        nb_fields = session.get("nb_fields", None)
        player_list = select_pre_calc_file(nb_players_t1, nb_fields)

        # on récupère les deux listes de joueurs
        level_one_players_t1 = player_list['group']['players']['levelOne']
        level_two_players_t1 = player_list['group']['players']['levelTwo']

        # on enregistre en session les deux listes
        session["level_one_players_t1"] = level_one_players_t1
        session["level_two_players_t1"] = level_two_players_t1

        # calcul des nombres min et max de matchs que feront les joueurs
        # sera utilisé pour calculer le classement
        # sur une base commune du min_match
        min_match = int(session.get("nb_series_wanted", None)/2)
        for elem in level_one_players_t1+level_two_players_t1:
            if elem['nb_match'] > min_match:
                elem['nb_match'] = min_match

        max_match = 0
        for elem in level_one_players_t1+level_two_players_t1:
            if elem['nb_match'] > max_match:
                max_match = elem['nb_match']
            if elem['nb_match'] < min_match:
                min_match = elem['nb_match']

        # on enregistre en session les deux listes
        session["min_match"] = min_match

        return render_template(
            'players_one_club_less_nbfields_t1.j2',
            level_one_players=level_one_players_t1,
            level_two_players=level_two_players_t1,
            group_name=player_list['group']['name'],
            player_names=player_names_t1)

    # request.method == 'POST':

    # on récupère le nom de chaque joueur qui aura été associé à
    # l'alias qui a servi pour le calcul des séries et du nombre
    # de matchs prévu
    level_one_players_t1 = session.get("level_one_players_t1", None)
    level_two_players_t1 = session.get("level_two_players_t1", None)
    nb_fields = session.get("nb_fields", None)

    nb_players_t1 = handling_player_data(level_one_players_t1,
                                         level_two_players_t1,
                                         nb_fields,
                                         '_fields_t1.yaml')
    session["nb_players_t1"] = nb_players_t1
    return redirect(url_for('players_alternat2_club'))


@MON_SERVEUR_WEB.route('/playersAlternat2Club', methods=['GET', 'POST'])
def players_alternat2_club():
    """
        gestion de la saisie des joueurs pour un double tournoi
        en alternance au sein d'un club
        GET = affichage des pages de saisie des joueurs
        POST = sauvegarde, traitement et affichage du menu suivant
    """

    if request.method == 'GET':
        # on récupère la liste des joueurs du tournoi 2
        player_names_t2 = read_players('players', 'player_names_t2.yaml')
        # on mélange les noms afin de mélanger l'ordre d'affichage
        # des joueurs
        shuffle(player_names_t2['levelOne'])
        shuffle(player_names_t2['levelTwo'])

        nb_players_t2 = 2*len(player_names_t2['levelOne'])
        nb_fields = session.get("nb_fields", None)
        player_list = select_pre_calc_file(nb_players_t2, nb_fields)

        # on récupère les deux listes de joueurs
        level_one_players_t2 = player_list['group']['players']['levelOne']
        level_two_players_t2 = player_list['group']['players']['levelTwo']

        # on enregistre en session les deux listes
        session["level_one_players_t2"] = level_one_players_t2
        session["level_two_players_t2"] = level_two_players_t2

        # calcul des nombres min et max de matchs que feront les joueurs
        # sera utilisé pour calculer le classement
        # sur une base commune du min_match
        min_match = int(session.get("nb_series_wanted", None)/2)
        for elem in level_one_players_t2+level_two_players_t2:
            if elem['nb_match'] > min_match:
                elem['nb_match'] = min_match

        max_match = 0
        for elem in level_one_players_t2+level_two_players_t2:
            if elem['nb_match'] > max_match:
                max_match = elem['nb_match']
            if elem['nb_match'] < min_match:
                min_match = elem['nb_match']

        # on enregistre en session les deux listes
        session["min_match"] = min_match

        return render_template(
            'players_one_club_less_nbfields_t2.j2',
            level_one_players=level_one_players_t2,
            level_two_players=level_two_players_t2,
            group_name=player_list['group']['name'],
            player_names=player_names_t2)

    # request.method == 'POST':

    # on récupère le nom de chaque joueur qui aura été associé à
    # l'alias qui a servi pour le calcul des séries et du nombre
    # de matchs prévu
    level_one_players_t2 = session.get("level_one_players_t2", None)
    level_two_players_t2 = session.get("level_two_players_t2", None)
    nb_fields = session.get("nb_fields", None)

    nb_players_t2 = handling_player_data(level_one_players_t2,
                                         level_two_players_t2,
                                         nb_fields,
                                         '_fields_t2.yaml')
    session["nb_players_t2"] = nb_players_t2
    tournament_style = session.get("tournament_style", None)
    nb_players_t1 = session.get("nb_players_t1", None)
    fichier_t1 = open(
        'precalc/' +
        str(nb_players_t1) +
        '_players_named_' +
        str(nb_fields) +
        '_fields_t1.yaml')
    fichier_t2 = open(
        'precalc/' +
        str(nb_players_t2) +
        '_players_named_' +
        str(nb_fields) +
        '_fields_t2.yaml')
    tour1 = yaml.safe_load(fichier_t1)
    tour2 = yaml.safe_load(fichier_t2)
    fichier_t1.close()
    fichier_t2.close()

    # creation des listes pour tournoi t1
    group_t1 = tour1['group']
    level_one_players_t1 = tour1['group']['players']['levelOne']
    level_two_players_t1 = tour1['group']['players']['levelTwo']
    session["level_one_players_t1"] = level_one_players_t1
    session["level_two_players_t1"] = level_two_players_t1
    # nb_players_level_t1 = len(lvl_1_players_t1)

    # creation des listes pour tournoi t2
    group_t2 = tour2['group']
    level_one_players_t2 = tour2['group']['players']['levelOne']
    level_two_players_t2 = tour2['group']['players']['levelTwo']
    session["level_one_players_t2"] = level_one_players_t2
    session["level_two_players_t2"] = level_two_players_t2
    # nb_players_level_t2 = len(lvl_1_players_t2)

    # la fonction calcul_players permet de créer
    # une liste de joueurs avec leurs caractéristiques
    players_all_t1 = calcul_players(group_t1)
    session["players_all_t1"] = players_all_t1

    for index, player in enumerate(players_all_t1):
        player['id'] = index+1

    # la fonction calcul_players permet de créer
    # une liste de joueurs avec leurs caractéristiques
    players_all_t2 = calcul_players(group_t2)
    session["players_all_t2"] = players_all_t2

    for index, player in enumerate(players_all_t2):
        player['id'] = index+1

    # on ouvre le fichier des series pre-calculées
    # par le programme test
    number_of_series = session.get("nb_series_wanted", None)
    if number_of_series not in (7, 10, 12, 14):
        number_of_series = 5
    
    elif number_of_series == 10:
        number_of_series = 5
    elif number_of_series == 12:
        number_of_series = 6
    elif number_of_series == 14:
        number_of_series = 7

    with open('precalc/' +
              str(nb_players_t2) +
              '_players_' +
              str(number_of_series) +
              '_series_' +
              str(nb_fields) +
              '_fields.json') as json_data:
        series = json.load(json_data)
    json_data.close()

    players_all_t1 = session.get("players_all_t1", None)
    players_all = players_all_t1 + players_all_t2
    level_one_players_t1 = session.get("level_one_players_t1", None)
    level_two_players_t1 = session.get("level_two_players_t1", None)
    level_one_players = level_one_players_t1 + level_one_players_t2
    level_two_players = level_two_players_t1 + level_two_players_t2

    session["players_all"] = players_all
    session["level_one_players"] = level_one_players
    session["level_two_players"] = level_two_players

    result_series_t1 = calculate_result_series(players_all_t1, series)
    result_series_t2 = calculate_result_series(players_all_t2, series)

    result_series = []
    i = 0
    while i < number_of_series:
        result_series.append(result_series_t1[i])
        result_series.append(result_series_t2[i])
        i = i+1

    series_reduced = list(result_series)
    session["result_series"] = result_series
    session["series_reduced"] = series_reduced
    result_series = session.get("result_series", None)
    tournament_style = session.get("tournament_style", None)

    return render_template(
        'welcome.j2',
        series=series_reduced,
        result_series=result_series,
        tournament_style=tournament_style)


@MON_SERVEUR_WEB.route('/playersByTeam/', methods=['GET'])
def players_by_team():
    """
        traitements affichage des joueurs par equipe
    """

    series_reduced = session.get("series_reduced", None)
    players_group1 = session.get("players_group1", None)
    players_group2 = session.get("players_group2", None)
    result_series = session.get("result_series", None)
    tournament_style = session.get("tournament_style", None)

    return render_template(
        'playersByTeam.j2',
        players_group1=players_group1,
        players_group2=players_group2,
        series=series_reduced,
        result_series=result_series,
        tournament_style=tournament_style)


@MON_SERVEUR_WEB.route('/playersByLevel/', methods=['GET'])
def players_by_level():
    """
        traitements affichage des joueurs par niveau
    """
    level_one_players = session.get("level_one_players", None)
    level_two_players = session.get("level_two_players", None)
    series_reduced = session.get("series_reduced", None)
    result_series = session.get("result_series", None)
    tournament_style = session.get("tournament_style", None)

    return render_template(
        'playersByLevel.j2',
        level_one_players=level_one_players,
        level_two_players=level_two_players,
        series=series_reduced,
        result_series=result_series,
        tournament_style=tournament_style)


@MON_SERVEUR_WEB.route('/serie<numserie>/', methods=['GET', 'POST'])
def serie(numserie):
    """
        traitements affichage d'une serie
    """

    series_reduced = session.get("series_reduced", None)
    players_all = session.get("players_all", None)
    result_series = session.get("result_series", None)
    tournament_style = session.get("tournament_style", None)
    level_one_players = session.get("level_one_players", None)
    level_two_players = session.get("level_two_players", None)
    players_group1 = session.get("players_group1", None)
    players_group2 = session.get("players_group2", None)

    if request.method == 'GET':
        players_that_serie = []
        players_in_rest = []
        for match in series_reduced[int(numserie)-1]:
            players_that_serie.append(match['teamA']['player1']['name'])
            players_that_serie.append(match['teamA']['player2']['name'])
            players_that_serie.append(match['teamB']['player1']['name'])
            players_that_serie.append(match['teamB']['player2']['name'])
        for player in players_all:
            if player['name'] not in players_that_serie:
                players_in_rest.append(player)
        return render_template(
            'serie.j2',
            serie_number=numserie,
            serie=series_reduced[int(numserie)-1],
            series=series_reduced,
            players_in_rest=players_in_rest,
            result_series=result_series,
            tournament_style=tournament_style)

    # request.method == 'POST':
    serie_elem = series_reduced[int(numserie)-1]
    result_serie = result_series[int(numserie)-1]
    num_match_in_that_serie = len(serie_elem)
    num_match = 0
    while num_match < num_match_in_that_serie:
        score_a = 'scoreA'+str(num_match+1)
        score_b = 'scoreB'+str(num_match+1)
        score_a_serie = request.form[score_a]
        score_b_serie = request.form[score_b]
        match = serie_elem[num_match]
        result_serie[num_match]['scoreA'] = score_a_serie
        result_serie[num_match]['scoreB'] = score_b_serie
        num_match = num_match+1
    session["result_serie"] = result_serie
    # sauvegarder les données de manière à redemarrer un tournoi
    # après arrêt PC
    with open('save/SERIES_REDUCED.json', 'w') as outfile:
        json.dump(series_reduced, outfile, indent=4)
    outfile.close()
    with open('save/RESULT_SERIES.json', 'w') as outfile:
        json.dump(result_series, outfile, indent=4)
    outfile.close()
    with open('save/players_all.json', 'w') as outfile:
        json.dump(players_all, outfile, indent=4)
    outfile.close()
    with open('save/players_group1.json', 'w') as outfile:
        json.dump(players_group1, outfile, indent=4)
    outfile.close()
    with open('save/players_group2.json', 'w') as outfile:
        json.dump(players_group2, outfile, indent=4)
    outfile.close()
    with open('save/level_one_players.json', 'w') as outfile:
        json.dump(level_one_players, outfile, indent=4)
    outfile.close()
    with open('save/level_two_players.json', 'w') as outfile:
        json.dump(level_two_players, outfile, indent=4)
    outfile.close()

    series_reduced = session.get("series_reduced", None)
    result_series = session.get("result_series", None)
    tournament_style = session.get("tournament_style", None)
    min_match = session.get("min_match", None)
    level_one_players = session.get("level_one_players", None)
    level_two_players = session.get("level_two_players", None)
    if session.get("tournament_style", None) == "1":
        player_list = read_players('players', 'player_names.yaml')
        l1_players = player_list['levelOne']
        l2_players = player_list['levelTwo']
        directory = session["tournament_directory"] + '/'
        record_names(l1_players, l2_players, directory, 'player_names.yaml')
    elif session.get("tournament_style", None) == "3":
        player_list = read_players('players', 'player_names_t1.yaml')
        l1_players = player_list['levelOne']
        l2_players = player_list['levelTwo']
        directory = session["tournament_directory"] + '/'
        record_names(l1_players, l2_players, directory, 'player_names_t1.yaml')
        player_list = read_players('players', 'player_names_t2.yaml')
        l1_players = player_list['levelOne']
        l2_players = player_list['levelTwo']
        directory = session["tournament_directory"] + '/'
        record_names(l1_players, l2_players, directory, 'player_names_t2.yaml')
    l1_sorted, l2_sorted = resultPlayersByLevel_calculation(series_reduced,
                                                            result_series,
                                                            tournament_style,
                                                            min_match,
                                                            level_one_players,
                                                            level_two_players)
    classement_l1_file = (session["tournament_directory"] +
                          '/' +
                          'classement_level1.json')
    classement_l2_file = (session["tournament_directory"] +
                          '/' +
                          'classement_level2.json')
    with open(classement_l1_file, 'w') as outfile:
        json.dump(l1_sorted, outfile, indent=4)
    with open(classement_l2_file, 'w') as outfile:
        json.dump(l2_sorted, outfile, indent=4)
    outfile.close()

    return render_template(
        'resultSerie.j2',
        serie_number=numserie,
        series=series_reduced,
        result_serie=result_series[int(numserie)-1],
        result_series=result_series,
        tournament_style=tournament_style)


@MON_SERVEUR_WEB.route('/resultSerie<numserie>/', methods=['GET'])
def resultat_serie(numserie):
    """
        traitements affichage du resultat d'une serie
    """
    series_reduced = session.get("series_reduced", None)
    result_series = session.get("result_series", None)
    tournament_style = session.get("tournament_style", None)

    players_that_serie = []
    result_serie = result_series[int(numserie)-1]
    for match in result_serie:
        players_that_serie.append(match['teamA']['player1']['name'])
        players_that_serie.append(match['teamA']['player2']['name'])
        players_that_serie.append(match['teamB']['player1']['name'])
        players_that_serie.append(match['teamB']['player2']['name'])
        return render_template(
            'resultSerie.j2',
            serie_number=numserie,
            series=series_reduced,
            result_serie=result_series[int(numserie)-1],
            result_series=result_series,
            tournament_style=tournament_style)


@MON_SERVEUR_WEB.route('/resultPlayersByTeam/', methods=['GET'])
def result_players_by_team():
    """
        traitements affichage classement et resultats des joueurs
        par équipes
    """
    # global NB_FIELDS
    # global players_all
    # global RESULT_SERIES
    # global SERIES_REDUCED
    # global players_group1
    # global players_group2
    # global level_one_players
    # global level_two_players
    series_reduced = session.get("series_reduced", None)
    players_all = session.get("players_all", None)
    result_series = session.get("result_series", None)
    tournament_style = session.get("tournament_style", None)
    players_group1 = session.get("players_group1", None)
    players_group2 = session.get("players_group2", None)

    for player in players_all:
        player['totalPointMarques'] = 0
        player['totalPointEncaisses'] = 0
        player['nbWins'] = 0
        player['nbLooses'] = 0
        player['nbJoues'] = 0
        player['diffPoints'] = 0
        player['rang'] = 0
    for result_serie in result_series:
        num_match_in_that_serie = len(result_serie)
        num_match = 0
        while num_match < num_match_in_that_serie:
            match = result_serie[num_match]
            player1name = match['teamA']['player1']['name']
            player2name = match['teamA']['player2']['name']
            player3name = match['teamB']['player1']['name']
            player4name = match['teamB']['player2']['name']
            for player in players_group1:
                if player1name == player['name']:
                    pl_m = player['totalPointMarques']
                    pl_e = player['totalPointEncaisses']
                    res_a = result_serie[num_match]['scoreA']
                    res_b = result_serie[num_match]['scoreB']
                    pl_m = pl_m + int(res_a)
                    pl_e = pl_e + int(res_b)
                    player['totalPointMarques'] = pl_m
                    player['totalPointEncaisses'] = pl_e
                    if res_a > res_b:
                        player['nbWins'] = player['nbWins']+1
                        player['nbJoues'] = player['nbJoues']+1
                    elif res_a < res_b:
                        player['nbLooses'] = player['nbLooses']+1
                        player['nbJoues'] = player['nbJoues']+1
                    player['diffPoints'] = pl_m - pl_e
                if player2name == player['name']:
                    pl_m = player['totalPointMarques']
                    pl_e = player['totalPointEncaisses']
                    res_a = result_serie[num_match]['scoreA']
                    res_b = result_serie[num_match]['scoreB']
                    pl_m = pl_m + int(res_a)
                    pl_e = pl_e + int(res_b)
                    player['totalPointMarques'] = pl_m
                    player['totalPointEncaisses'] = pl_e
                    if res_a > res_b:
                        player['nbWins'] = player['nbWins']+1
                        player['nbJoues'] = player['nbJoues']+1
                    elif res_a < res_b:
                        player['nbLooses'] = player['nbLooses']+1
                        player['nbJoues'] = player['nbJoues']+1
                    player['diffPoints'] = pl_m - pl_e
            for player in players_group2:
                if player3name == player['name']:
                    pl_m = player['totalPointMarques']
                    pl_e = player['totalPointEncaisses']
                    res_a = result_serie[num_match]['scoreA']
                    res_b = result_serie[num_match]['scoreB']
                    pl_m = pl_m + int(res_b)
                    pl_e = pl_e + int(res_a)
                    player['totalPointMarques'] = pl_m
                    player['totalPointEncaisses'] = pl_e
                    if res_a < res_b:
                        player['nbWins'] = player['nbWins']+1
                        player['nbJoues'] = player['nbJoues']+1
                    elif res_a > res_b:
                        player['nbLooses'] = player['nbLooses']+1
                        player['nbJoues'] = player['nbJoues']+1
                    player['diffPoints'] = pl_m - pl_e
                if player4name == player['name']:
                    pl_m = player['totalPointMarques']
                    pl_e = player['totalPointEncaisses']
                    res_a = result_serie[num_match]['scoreA']
                    res_b = result_serie[num_match]['scoreB']
                    pl_m = pl_m + int(res_b)
                    pl_e = pl_e + int(res_a)
                    player['totalPointMarques'] = pl_m
                    player['totalPointEncaisses'] = pl_e
                    if res_a < res_b:
                        player['nbWins'] = player['nbWins']+1
                        player['nbJoues'] = player['nbJoues']+1
                    elif res_a > res_b:
                        player['nbLooses'] = player['nbLooses']+1
                        player['nbJoues'] = player['nbJoues']+1
                    player['diffPoints'] = pl_m - pl_e
            num_match = num_match+1
    players_group1_sorted = sorted(
        players_group1,
        key=itemgetter('nbWins', 'diffPoints'),
        reverse=True)
    players_group2_sorted = sorted(
        players_group2,
        key=itemgetter('nbWins', 'diffPoints'),
        reverse=True)
    total_group1 = 0
    for player in players_group1:
        total_group1 = total_group1+player['nbWins']
    total_group2 = 0
    for player in players_group2:
        total_group2 = total_group2+player['nbWins']

    return render_template(
        'resultPlayersByTeam.j2',
        total_group1=total_group1/2,
        total_group2=total_group2/2,
        players_group1=players_group1_sorted,
        players_group2=players_group2_sorted,
        series=series_reduced,
        result_series=result_series,
        tournament_style=tournament_style)


@MON_SERVEUR_WEB.route('/resultPlayersByLevel/', methods=['GET'])
def result_players_by_level():
    """
        traitements affichage classement et resultats des joueurs
        par niveau
    """
    series_reduced = session.get("series_reduced", None)
    result_series = session.get("result_series", None)
    tournament_style = session.get("tournament_style", None)
    min_match = session.get("min_match", None)
    level_one_players = session.get("level_one_players", None)
    level_two_players = session.get("level_two_players", None)

    l1_sorted, l2_sorted = resultPlayersByLevel_calculation(series_reduced,
                                                            result_series,
                                                            tournament_style,
                                                            min_match,
                                                            level_one_players,
                                                            level_two_players)

    return render_template(
        'resultPlayersByLevel.j2',
        level_one_players=l1_sorted,
        level_two_players=l2_sorted,
        series=series_reduced,
        result_series=result_series,
        tournament_style=tournament_style,
        min_match=min_match)


@MON_SERVEUR_WEB.route('/minuteur<minute>/', methods=['GET'])
def minuteur(minute):
    """
        traitements affichage d'un minuteur duree de match
    """
    return render_template(
        'minuteur.j2',
        minute=minute)


if __name__ == '__main__':
    MON_SERVEUR_WEB.run(host="0.0.0.0", debug=True)
