# Gestion d'un tournoi amical de badminton

Ce programme permet d'aider à l'organisation d'un tournoi de badminton en mode amical

Le programme implemente les contraintes suivantes :

- un joueur ne doit pas se retrouver à devoir jouer sur deux terrains dans
  la même série
- chaque joueur doit avoir effectué à peu près le même nombre de matchs que
  les autres à la fin du tournoi
- chaque joueur change de partenaire à chaque match
- chaque joueur change d'adversaires à chaque match (dans la mesure du possible)

Les joueurs ne décident donc pas de leur partenaire et ils changent à chaque tour.

Ce programme n'est donc pas adapté à l'organisation d'un tournoi selon
les règles internationales officielles.

L'organisateur du tournoi indique le nombre de joueurs et le nombre de serie
qu'il souhaite

![Ecran principal](Tournoi/images/ecran5.png)

## INSTALLATION

pré-requis : disposer d'un PC avec les logiciels python3, pip3 et git

cloner le programme sur votre PC en utilisant le logiciel git

```bash
git clone https://gitlab.com/Rene_ROBERT/badmintonrospez.git
```

vous devez obtenir un répertoire badmintonrospez qui contient deux
sous-repertoire contenant chacun un programme python.

Dans le repertoire CalculateSeries, vous trouverez un programme qui pre-calcul
toutes les series et les rencontres en fonction du nombre de joueurs, du nombre
de terrains et du nombre de serie souhaitées.

Ce programme génère des fichiers Json. Une fois les fichiers obtenus, il faudra
les recopier et les utiliser par le programme Tournoi.

Normalement, si le resultat est satisfaisant, il n'est plus necessaire
d'utiliser CalculateSerie par la suite.

Dans le répertoire Tournoi, vous trouverez le programme de gestion d'un Tournoi
de badminton en mode amical.

Il s'agit du programme principal, qui utilise les fichiers pre-calculés
par CalculateSeries, et qui va gérer la prise en compte des noms de joueurs,
la configuration du tournoi (nombre de terrains, nombre de series souhaitées,
type de Tournoi...), les menus et calculs des résulats...

Vous utiliserez le programme Tournoi pour chacun de vos tournois amicaux.
