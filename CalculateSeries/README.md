# BadmintonRospez-CalculateSeries

ce programme python permet de calculer les series d'un tournoi de badminton
en mode amical

Une serie est un ensemble de match de badminton, en double, qui vont
se jouer simultanément.

Si vous avez 24 joueurs et 5 terrains, vous aurez une serie de 5 matchs avec
4 joueurs en attente de la prochaine série.

En tant qu'organisateur, vous pouvez décider que le tournoi se déroulera en
10 series par exemple.

Le principe d'un tournoi amical tel que nous l'avons imaginé est le suivant :

- il s'agit d'un tournoi en double
- chaque joueur va changer de partenaire à chaque série et ne devrait jamais
  rejouer avec la meme personne
- il y aura un joueur "confirmé" associé à un joueur "moins confirmé"
- un joueur en attente (faute de terrain disponible), jouera la serie suivante

Ces contraintes engendrent un algorithme complexe. Le programme CalculateSeries
tente d'y répondre.

Le programme va générer des fichiers contenant les series avec des joueurs
"fictifs".

Le programme "Tournoi" utilise ce genre de fichiers et ensuite utilise
les vrais noms des joueurs de votre tournoi.

Le programme "Tournoi" dispose déjà des fichiers pre-calculés, vous ne devriez
donc pas avoir besoin de les générer.
