#!/usr/bin/env python3
# coding: utf-8
"""
    programme de calcul des series d'un tournoi au sein d'un club
    avec changement des équipes à chaque serie
"""
from random import shuffle
import json
import yaml


def compare_team_matchs(match1, match2):
    """
    Objet de la fonction :
        verification qu'une même équipe est présente (ou non)
        entre deux matchs que l'on compare
    Paramètres en entrée et rôle :
        match1 et match2 : les deux matchs que l'on va comparer
    Paramètres en sortie et rôle :
        positif/négatif : la comparaison est positive si une même
        équipe se retrouve entre deux matchs
    """
    team1_m1 = (match1[0], match1[1])
    team2_m1 = (match1[2], match1[3])
    team3_m1 = (match1[1], match1[0])
    team4_m1 = (match1[3], match1[2])
    team1_m2 = (match2[0], match2[1])
    team2_m2 = (match2[2], match2[3])
    team3_m2 = (match2[1], match2[0])
    team4_m2 = (match2[3], match2[2])

    list1 = [team1_m1, team2_m1, team3_m1, team4_m1]
    list2 = [team1_m2, team2_m2, team3_m2, team4_m2]
    ens1 = set(list1)
    ens2 = set(list2)

    return bool(ens1.intersection(ens2))


def compare_opposants_matchs(match1, match2):
    """
    Objet de la fonction :
        verification qu'une même opposition entre deux joueurs
       est présente (ou non) entre deux matchs que l'on compare
    Paramètres en entrée et rôle :
        match1 et match2 : les deux matchs que l'on va comparer
    Paramètres en sortie et rôle :
        positif/négatif : la comparaison est positive si une même
        opposition se retrouve entre deux matchs
    """

    opposants1_m1 = (match1[0], match1[2])
    opposants2_m1 = (match1[0], match1[3])
    opposants3_m1 = (match1[1], match1[2])
    opposants4_m1 = (match1[1], match1[3])

    opposants5_m1 = (match1[2], match1[0])
    opposants6_m1 = (match1[3], match1[0])
    opposants7_m1 = (match1[2], match1[1])
    opposants8_m1 = (match1[3], match1[1])

    opposants1_m2 = (match2[0], match2[2])
    opposants2_m2 = (match2[0], match2[3])
    opposants3_m2 = (match2[1], match2[2])
    opposants4_m2 = (match2[1], match2[3])

    opposants5_m2 = (match2[2], match2[0])
    opposants6_m2 = (match2[3], match2[0])
    opposants7_m2 = (match2[2], match2[1])
    opposants8_m2 = (match2[3], match2[1])

    list1 = [
        opposants1_m1,
        opposants2_m1,
        opposants3_m1,
        opposants4_m1,
        opposants5_m1,
        opposants6_m1,
        opposants7_m1,
        opposants8_m1]
    list2 = [
        opposants1_m2,
        opposants2_m2,
        opposants3_m2,
        opposants4_m2,
        opposants5_m2,
        opposants6_m2,
        opposants7_m2,
        opposants8_m2]
    ens1 = set(list1)
    ens2 = set(list2)

    return bool(ens1.intersection(ens2))


def compare_team_series(serie1, serie2):
    """
    Objet de la fonction :
        verification qu'une même équipe est présente (ou non)
        entre deux series que l'on compare
    Paramètres en entrée et rôle :
        serie1 et serie2 : les deux series que l'on va comparer
    Paramètres en sortie et rôle :
        positif/négatif : la comparaison est positive si une même équipe
        joue dans les deux series
    """
    test = False
    for match1 in serie1:
        for match2 in serie2:
            test = compare_team_matchs(match1, match2)
            if test is True:
                break
        if test is True:
            break
    return bool(test)


def compare_opposants_series(serie1, serie2):
    """
    Objet de la fonction :
        verification qu'une même opposition entre deux joueurs
        est présente (ou non) entre deux series que l'on compare
    Paramètres en entrée et rôle :
        serie1 et serie2 : les deux series que l'on va comparer
    Paramètres en sortie et rôle :
        positif/négatif : la comparaison est positive si deux joueurs
        se rencontrent dans les deux séries
    """
    test = False
    for match1 in serie1:
        for match2 in serie2:
            test = compare_opposants_matchs(match1, match2)
            if test is True:
                break
        if test is True:
            break
    return bool(test)


def calculate_teams(list1p, list2p):
    """
    Objet de la fonction :
        doubles possibles (un fort et un moins fort) dans un tournoi
        au sein d'un seul club avec deux listes de joueurs
    Paramètres en entrée et rôle :
        list1p : liste des joueurs de niveau 1 (les "forts" par exemple)
        list2p : liste des joueurs de niveau 2 (les "moins forts")
    Paramètres en sortie et rôle :
        teams : une liste de doubles, chaque double étant
            une liste de 2 joueurs : un niveau 1 et un niveau 2
    """
    teams = []
    for player1 in list1p:
        for player2 in list2p:
            team = [player1, player2]
            teams.append(team)
    return teams


def calculate_matchs(teams):
    """
    Objet de la fonction :
        construit la liste des matchs possible dans un tournoi
        au sein d'un seul club en évitant qu'un joueur
        soit des deux côtés du filet
        en évitant de refaire deux fois le même match
    Paramètres en entrée et rôle :
        nb_players : permet de calculer le nombre de matchs
                        dans la serie quand on ne remplit pas tous les
                        terrains
        teams : liste des doubles possibles dans laquelle puiser pour
            construire un match
    Paramètres en sortie et rôle :
        matchs : une liste de matchs, chaque match étant
            une liste de 4 joueurs
    """
    matchs = []
    teams_a = list(teams)
    teams_b = list(teams)
    for team_a in teams_a:
        for team_b in teams_b:
            if team_a[0] not in [team_b[0], team_b[1]]:
                if team_a[1] not in [team_b[0], team_b[1]]:
                    new_match = [team_a[0], team_a[1], team_b[0], team_b[1]]
                    matchs.append(new_match)
    for match1 in matchs:
        match2 = [match1[2], match1[3], match1[0], match1[1]]
        matchs.remove(match2)
    return matchs


def verif_nb_matchs_per_player(number_of_series,
                               series,
                               l1ps,
                               l2ps):
    """
    Objet de la fonction :
        verification du nombre de match que jouera un joueur
        vérification de l'écart entre celui qui fera le plus de match et
        celui qui en fera le moins
    Paramètres en entrée et rôle :
        number_of_series : nombre de series souhaitées
        list_of_list : liste des joueurs de niveau 1
        l2ps : liste des joueurs de niveau 2
    Paramètres en sortie et rôle :
        players_verif : liste des joueurs avec leur nombre de matchs
        et écarts max et min
    """
    print("debut verif")
    max1 = 0
    min1 = number_of_series
    players_verif = {}
    players_verif['players'] = []
    players_verif['maxi'] = max1
    players_verif['mini'] = min1
    player = {}
    player['alias'] = ''
    player['nb_match'] = 0
    nb_match = 0

    for player1 in l1ps:
        # calcul du nombre de match
        for serie in series:
            for match in serie:
                for player2 in match:
                    if player2 == player1:
                        nb_match = nb_match + 1
        player = {}
        player['alias'] = player1
        player['name'] = player1
        player['nb_match'] = nb_match
        print(nb_match)
        players_verif['players'].append(player)
        if nb_match > max1:
            max1 = nb_match
        if nb_match < min1:
            min1 = nb_match
        nb_match = 0

    players_verif['maxi'] = max1
    players_verif['mini'] = min1

    for player1 in l2ps:
        for serie in series:
            for match in serie:
                for player2 in match:
                    if player2 == player1:
                        nb_match = nb_match + 1
        player = {}
        player['alias'] = player1
        player['name'] = player1
        player['nb_match'] = nb_match
        players_verif['players'].append(player)
        if nb_match > max1:
            max1 = nb_match
        if nb_match < min1:
            min1 = nb_match
        nb_match = 0
    players_verif['maxi'] = max1
    players_verif['mini'] = min1
    return players_verif


def create_file_series(nb_fields, l1_players, number_of_series, series):
    """
    creation du fichier des series
    """
    for idx, serie in enumerate(series):
        print(idx + 1, ' ', serie)
    with open('results/' +
              str(len(l1_players) + len(l1_players)) +
              '_players_' +
              str(number_of_series) +
              '_series_' +
              str(nb_fields) +
              '_fields.json', 'w') as outfile:
        json.dump(series, outfile, indent=4)
    outfile.close()


def create_file_players(nb_fields, l1_players, verif):
    """
    creation du fichier des joueurs, par niveau
    et avec le nombre de matchs que chaque joueur fera
    """
    with open('results/' +
              str(len(l1_players) + len(l1_players)) +
              '_players_named_' +
              str(nb_fields) +
              '_fields.yaml', 'w') as outfile:
        group_name = 'ROSPEZ'
        new_group = {}
        new_group['group'] = {}
        new_group['group']['name'] = group_name
        new_group['group']['players'] = {}
        new_group['group']['players']['levelOne'] = []
        new_group['group']['players']['levelTwo'] = []
        for idx, player in enumerate(verif['players']):
            if idx < len(l1_players):
                new_group['group']['players']['level\
One'].append(player)
            if idx >= len(l1_players):
                new_group['group']['players']['level\
Two'].append(player)
        yaml.dump(
            new_group,
            outfile,
            default_flow_style=False,
            allow_unicode=True)
    outfile.close()


def verif_per_player(l1_players, l2_players, series):
    """
    verification que chaque joueur joue avec
    plusieurs autres joueurs et pas toujours les mêmes
    """
    print(' les partenaires de double : ')

    partenaires = []
    for player in l1_players:
        for serie in series:
            for match in serie:
                if player == match[0]:
                    partenaires.append(match[1])
                if player == match[2]:
                    partenaires.append(match[3])
        for partenaire in partenaires:
            print(player, partenaire +
                  '*' + str(partenaires.count(partenaire)))
        partenaires = []

    for player in l2_players:
        for serie in series:
            for match in serie:
                if player == match[1]:
                    partenaires.append(match[0])
                if player == match[3]:
                    partenaires.append(match[2])
        print(player, partenaires)
        partenaires = []

    print(' les opposants : ')

    opposants = []
    for player in l1_players:
        for serie in series:
            for match in serie:
                if player == match[0]:
                    opposants.append(match[2])
                    opposants.append(match[3])
                if player == match[2]:
                    opposants.append(match[0])
                    opposants.append(match[1])
        print(player, opposants)
        opposants = []

    opposants = []
    for player in l2_players:
        for serie in series:
            for match in serie:
                if player == match[1]:
                    opposants.append(match[2])
                    opposants.append(match[3])
                if player == match[3]:
                    opposants.append(match[0])
                    opposants.append(match[1])
        print(player, opposants)
        opposants = []


def calculate_one_serie(nb_players,
                        matchs,
                        fields):
    """
    Objet de la fonction :
        calcul d'une serie en évitant de programmer un joueur sur
        plusieurs terrains dans la série (on compare par intersection
        la liste des joueurs du match envisagé avec la liste des joueurs
        de la serie déjà retenus
    Paramètres en entrée et rôle :
        nb_players : permet de calculer le nombre de matchs
                        dans la serie quand on ne remplit pas tous les
                        terrains
        fields : permet de calculer le nombre de matchs maximum
                    possibles quand le nombre de joueur dépasse les
                    capacités de la salle
        matchs : liste des matchs dans laquelle puiser pour construire
                    une série
    Paramètres en sortie et rôle :
        serie : une serie constituée d'une liste de matchs
            dont le nombre dépend soit du nombre de joueurs
            soit du nombre de terrains
    """
    serie = []
    acceptable = False
    while acceptable is False:
        for match1 in matchs:
            match1_set = set(match1)
            pas_bon = False
            for match2 in serie:
                match2_set = set(match2)
                if match1_set.intersection(match2_set) != set():
                    pas_bon = True
            if pas_bon is False:
                serie.append(match1)
                if len(serie) == fields or len(serie) == nb_players / 4:
                    return serie


def calculate_several_series(number_of_series,
                             nb_players,
                             matchs,
                             fields):
    """
    Objet de la fonction :
        calcul de plusieurs series
    Paramètres en entrée et rôle :
        number_of_series : nombre de series souhaitées
        nb_players : permet de calculer le nombre de matchs
                        dans la serie quand on ne remplit pas tous les
                        terrains
        fields : permet de calculer le nombre de matchs maximum
                    possibles quand le nombre de joueur dépasse les
                    capacités de la salle
        matchs : liste des matchs dans laquelle puiser pour construire
                    les séries
    Paramètres en sortie et rôle :
        series : un ensemble de series
    """
    # calcul de la 1ere serie
    series = []
    serie = calculate_one_serie(nb_players,
                                matchs,
                                fields)
    series.append(serie)
    number_of_try = 0
    while len(series) < number_of_series:
        serie = calculate_one_serie(nb_players,
                                    matchs,
                                    fields)
        if len(series)+1 < 10:
            # print("strict mode try : ", number_of_try)
            if check_serie_strict(serie, series):
                series.append(serie)
        # on élimines les series qui apparaissent plusieurs fois
            series1 = list(series)
            for serie1 in series1:
                if series.count(serie1) > 1:
                    series.remove(serie1)
        # y compris quand les matchs sont inversés
            for serie1 in series1:
                m1_s1 = serie1[0]
                m2_s1 = serie1[1]
                serie1_inverse = []
                serie1_inverse.append(m2_s1)
                serie1_inverse.append(m1_s1)
                if series.count(serie1_inverse) > 1:
                    series.remove(serie1)
            # on mélange les matchs pour la prochaine recherche de serie
            shuffle(matchs)
        else:
            print("not strict mode try : ", number_of_try)
            if check_serie_not_strict(serie, series):
                series.append(serie)
        # on élimines les series qui apparaissent plusieurs fois
            series1 = list(series)
            for serie1 in series1:
                if series.count(serie1) > 1:
                    series.remove(serie1)
        # y compris quand les matchs sont inversés
            for serie1 in series1:
                m1_s1 = serie1[0]
                m2_s1 = serie1[1]
                serie1_inverse = []
                serie1_inverse.append(m2_s1)
                serie1_inverse.append(m1_s1)
                if series.count(serie1_inverse) > 1:
                    series.remove(serie1)
            shuffle(matchs)
        number_of_try = number_of_try + 1
    return series


def check_serie_strict(serie, series):
    """
    verification que l'on ne fait pas jouer 2 fois la même équipe
    durant le tournoi
    """
    elems = []
    for elem in series:
        for equipe in equipes(elem):
            elems.append(equipe)
    for equipe in equipes(serie):
        elems.append(equipe)
    for elem in elems:
        if elems.count(elem) > 1:
            return False
    # print("serie OK")
    # print("serie :", serie)
    # print("series :", series)
    print("nombre de series compatibles : ", len(series)+1)
    return True


def check_serie_not_strict(serie, series):
    """
    On autorise deux joueurs à rejouer
    """
    elems = []
    for elem in series:
        for equipe in equipes(elem):
            elems.append(equipe)
    for equipe in equipes(serie):
        elems.append(equipe)
    for elem in elems:
        if elems.count(elem) > 2:
            return False
    # print("serie OK")
    # print("serie :", serie)
    # print("series :", series)
    print("nombre de series compatibles : ", len(series)+1)
    return True


def equipes(serie):
    """
    produit une liste des équipes d'une série
    """
    doubles = []
    for match in serie:
        equipe1 = []
        equipe2 = []
        equipe3 = []
        equipe4 = []
        equipe1.append(match[0])
        equipe1.append(match[1])
        equipe2.append(match[2])
        equipe2.append(match[3])
        equipe3.append(match[1])
        equipe3.append(match[0])
        equipe4.append(match[3])
        equipe4.append(match[2])
        doubles.append(equipe1)
        doubles.append(equipe2)
        doubles.append(equipe3)
        doubles.append(equipe4)
    return doubles


def main():
    """
    fonction principale
    """
    l1_16_players = [
        '1', '2', '3', '4',
        '5', '6', '7', '8']
    l2_16_players = [
        'A', 'B', 'C', 'D',
        'E', 'F', 'G', 'H']
    l_16_players = [l1_16_players, l2_16_players]

    l1_20_players = [
        '1', '2', '3', '4',
        '5', '6', '7', '8',
        '9', '10']
    l2_20_players = [
        'A', 'B', 'C', 'D',
        'E', 'F', 'G', 'H',
        'I', 'J']
    l_20_players = [l1_20_players, l2_20_players]

    l1_24_players = [
        '1', '2', '3', '4',
        '5', '6', '7', '8',
        '9', '10', '11', '12']
    l2_24_players = [
        'A', 'B', 'C', 'D',
        'E', 'F', 'G', 'H',
        'I', 'J', 'K', 'L']
    l_24_players = [l1_24_players, l2_24_players]

    l1_28_players = [
        '1', '2', '3', '4',
        '5', '6', '7', '8',
        '9', '10', '11', '12',
        '13', '14']
    l2_28_players = [
        'A', 'B', 'C', 'D',
        'E', 'F', 'G', 'H',
        'I', 'J', 'K', 'L',
        'M', 'N']
    l_28_players = [l1_28_players, l2_28_players]

    l1_32_players = [
        '1', '2', '3', '4',
        '5', '6', '7', '8',
        '9', '10', '11', '12',
        '13', '14', '15', '16']
    l2_32_players = [
        'A', 'B', 'C', 'D',
        'E', 'F', 'G', 'H',
        'I', 'J', 'K', 'L',
        'M', 'N', 'O', 'P']
    l_32_players = [l1_32_players, l2_32_players]

    l1_36_players = [
        '1', '2', '3', '4',
        '5', '6', '7', '8',
        '9', '10', '11', '12',
        '13', '14', '15', '16',
        '17', '18']
    l2_36_players = [
        'A', 'B', 'C', 'D',
        'E', 'F', 'G', 'H',
        'I', 'J', 'K', 'L',
        'M', 'N', 'O', 'P',
        'Q', 'R']
    l_36_players = [l1_36_players, l2_36_players]

    # l1_40_players = [
    #     '1', '2', '3', '4',
    #     '5', '6', '7', '8',
    #     '9', '10', '11', '12',
    #     '13', '14', '15', '16',
    #     '17', '18']
    # l2_40_players = [
    #     'A', 'B', 'C', 'D',
    #     'E', 'F', 'G', 'H',
    #     'I', 'J', 'K', 'L',
    #     'M', 'N', 'O', 'P',
    #     'Q', 'R']
    # l_40_players = [l1_40_players, l2_40_players]

    # list_nb_fields = [5, 6, 7, 8, 9]

    # List_of_list = [
    #   L_20_PLAYERS,
    #   L_24_PLAYERS,
    #   L_28_PLAYERS,
    #   L_36_PLAYERS,
    #   L_40_PLAYERS]

    l1_players = l_32_players[0]
    l2_players = l_32_players[1]
    print("BONJOUR")
    print('les joueurs : ')
    print(l1_players)
    print(l2_players)
    print('\n')

    nb_players = len(l1_players) + len(l2_players)

    # calcul des double&s possibles
    teams = calculate_teams(l1_players, l2_players)
    print("nombre de duo possibles : ", len(teams))
    # calcul des matchs possibles
    matchs = calculate_matchs(teams)
    print("nbre de matchs possibles avec tous ces duos: ", len(matchs))

    # calcul de 10 series possibles
    # une serie est un ensemble de matchs simultanés
    number_of_series = 10
    nb_fields = 5
    verif = {}
    verif['maxi'] = number_of_series
    verif['mini'] = 0
    try_nb = 1
    # la fonction "verif" sert à s'assurer que tout le monde
    # fait à peu près le même nombre de matchs
    while (verif['maxi'] - verif['mini']) > 3:
        series = calculate_several_series(
            number_of_series,
            nb_players,
            matchs,
            nb_fields)
        print("verif series")
        verif = verif_nb_matchs_per_player(
            number_of_series,
            series,
            l1_players,
            l2_players)
        print('full recalcul : ', try_nb)
        try_nb = try_nb + 1
    create_file_series(nb_fields, l1_players, number_of_series, series)
    create_file_players(nb_fields, l1_players, verif)
    verif_per_player(l1_players, l2_players, series)


if __name__ == "__main__":
    main()
