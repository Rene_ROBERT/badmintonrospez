#!/usr/bin/env python3
# coding: utf-8
"""
    programme de calcul des series d'un tournoi au sein d'un club
    avec changement des équipes à chaque serie
"""
import unittest
from calculate import compare_team_matchs,\
                    compare_opposants_matchs,\
                    compare_team_series,\
                    compare_opposants_series,\
                    calculate_teams,\
                    calculate_matchs,\
                    calculate_one_serie


class BadmintonTest(unittest.TestCase):
    """
    Test case utilisé pour tester les fonctions
    """

    def test_compare_team_matchs(self):
        """
        Test le fonctionnement de la fonction
            'compare_team_matchs(match1, match2)'.
        """
        match1 = ['1', 'A', '2', 'B']
        match2 = ['3', 'C', '4', 'D']
        match3 = ['1', 'A', '4', 'D']
        match4 = ['3', 'C', '2', 'B']

        test_teams = compare_team_matchs(match1, match2)
        self.assertEqual(test_teams, False)

        test_teams = compare_team_matchs(match1, match3)
        self.assertEqual(test_teams, True)

        test_teams = compare_team_matchs(match1, match4)
        self.assertEqual(test_teams, True)

    def test_compare_opposants_matchs(self):
        """
        Test le fonctionnement de la fonction
            'compare_opposants_matchs(m1, m2)'.
        """
        match1 = ['1', 'A', '2', 'B']
        match2 = ['3', 'C', '4', 'D']
        match3 = ['1', 'B', '2', 'C']
        match4 = ['2', 'C', '1', 'D']
        match5 = ['3', 'A', '4', 'B']

        test_opposants = compare_opposants_matchs(match1, match2)
        self.assertEqual(test_opposants, False)

        test_opposants = compare_opposants_matchs(match1, match3)
        self.assertEqual(test_opposants, True)

        test_opposants = compare_opposants_matchs(match1, match4)
        self.assertEqual(test_opposants, True)

        test_opposants = compare_opposants_matchs(match1, match5)
        self.assertEqual(test_opposants, True)

    def test_compare_team_series(self):
        """
        Test le fonctionnement de la fonction
            'compare_team_series(serie1, serie2)'.
        """
        serie1 = [['1', 'A', '2', 'B'], ['3', 'C', '4', 'D']]
        serie2 = [['1', 'B', '2', 'C'], ['3', 'D', '4', 'A']]
        serie3 = [['1', 'A', '2', 'C'], ['3', 'D', '4', 'B']]
        serie4 = [['3', 'B', '2', 'D'], ['1', 'A', '4', 'C']]

        test_teams = compare_team_series(serie1, serie2)
        self.assertEqual(test_teams, False)

        test_teams = compare_team_series(serie1, serie3)
        self.assertEqual(test_teams, True)

        test_teams = compare_team_series(serie1, serie4)
        self.assertEqual(test_teams, True)

    def test_compare_opposants_series(self):
        """
        Test le fonctionnement de la fonction
            'compare_opposants_series(serie1, serie2)'.
        """
        serie1 = [['1', 'A', '2', 'B'], ['3', 'C', '4', 'D']]
        serie2 = [['1', 'B', '3', 'C'], ['4', 'D', '2', 'A']]
        serie3 = [['1', 'B', '2', 'C'], ['3', 'D', '4', 'A']]

        test_opposants = compare_opposants_series(serie1, serie2)
        self.assertEqual(test_opposants, False)

        test_opposants = compare_opposants_series(serie1, serie3)
        self.assertEqual(test_opposants, True)

    def test_calculate_teams(self):
        """
        Test le fonctionnement de la fonction
            'calculate_teams(liste_player1, liste_player2)'.
        """
        liste_player1 = ['1', '2', '3', '4']
        liste_player2 = ['A', 'B', 'C', 'D']
        result_ok1 = [
            ['1', 'A'], ['1', 'B'], ['1', 'C'], ['1', 'D'],
            ['2', 'A'], ['2', 'B'], ['2', 'C'], ['2', 'D'],
            ['3', 'A'], ['3', 'B'], ['3', 'C'], ['3', 'D'],
            ['4', 'A'], ['4', 'B'], ['4', 'C'], ['4', 'D']
        ]

        test_teams = calculate_teams(liste_player1, liste_player2)
        self.assertEqual(test_teams, result_ok1)

    def test_calculate_matchs(self):
        """
        Test le fonctionnement de la fonction
            'calculate_matchs(nb_players, teams)'.
        """
        teams = [
            ['1', 'A'], ['1', 'B'], ['1', 'C'], ['1', 'D'],
            ['2', 'A'], ['2', 'B'], ['2', 'C'], ['2', 'D'],
            ['3', 'A'], ['3', 'B'], ['3', 'C'], ['3', 'D'],
            ['4', 'A'], ['4', 'B'], ['4', 'C'], ['4', 'D']
        ]
        result_ok1 = [
            ['1', 'A', '2', 'B'], ['1', 'A', '2', 'C'],
            ['1', 'A', '2', 'D'], ['1', 'A', '3', 'B'],
            ['1', 'A', '3', 'C'], ['1', 'A', '3', 'D'],
            ['1', 'A', '4', 'B'], ['1', 'A', '4', 'C'],
            ['1', 'A', '4', 'D'], ['1', 'B', '2', 'A'],
            ['1', 'B', '2', 'C'], ['1', 'B', '2', 'D'],
            ['1', 'B', '3', 'A'], ['1', 'B', '3', 'C'],
            ['1', 'B', '3', 'D'], ['1', 'B', '4', 'A'],
            ['1', 'B', '4', 'C'], ['1', 'B', '4', 'D'],
            ['1', 'C', '2', 'A'], ['1', 'C', '2', 'B'],
            ['1', 'C', '2', 'D'], ['1', 'C', '3', 'A'],
            ['1', 'C', '3', 'B'], ['1', 'C', '3', 'D'],
            ['1', 'C', '4', 'A'], ['1', 'C', '4', 'B'],
            ['1', 'C', '4', 'D'], ['1', 'D', '2', 'A'],
            ['1', 'D', '2', 'B'], ['1', 'D', '2', 'C'],
            ['1', 'D', '3', 'A'], ['1', 'D', '3', 'B'],
            ['1', 'D', '3', 'C'], ['1', 'D', '4', 'A'],
            ['1', 'D', '4', 'B'], ['1', 'D', '4', 'C'],
            ['2', 'A', '3', 'B'], ['2', 'A', '3', 'C'],
            ['2', 'A', '3', 'D'], ['2', 'A', '4', 'B'],
            ['2', 'A', '4', 'C'], ['2', 'A', '4', 'D'],
            ['2', 'B', '3', 'A'], ['2', 'B', '3', 'C'],
            ['2', 'B', '3', 'D'], ['2', 'B', '4', 'A'],
            ['2', 'B', '4', 'C'], ['2', 'B', '4', 'D'],
            ['2', 'C', '3', 'A'], ['2', 'C', '3', 'B'],
            ['2', 'C', '3', 'D'], ['2', 'C', '4', 'A'],
            ['2', 'C', '4', 'B'], ['2', 'C', '4', 'D'],
            ['2', 'D', '3', 'A'], ['2', 'D', '3', 'B'],
            ['2', 'D', '3', 'C'], ['2', 'D', '4', 'A'],
            ['2', 'D', '4', 'B'], ['2', 'D', '4', 'C'],
            ['3', 'A', '4', 'B'], ['3', 'A', '4', 'C'],
            ['3', 'A', '4', 'D'], ['3', 'B', '4', 'A'],
            ['3', 'B', '4', 'C'], ['3', 'B', '4', 'D'],
            ['3', 'C', '4', 'A'], ['3', 'C', '4', 'B'],
            ['3', 'C', '4', 'D'], ['3', 'D', '4', 'A'],
            ['3', 'D', '4', 'B'], ['3', 'D', '4', 'C']
        ]

        test_matchs = calculate_matchs(teams)
        self.assertEqual(test_matchs, result_ok1)

    def calculate_one_serie(self):
        """
        Test le fonctionnement de la fonction
            'calculate_one_serie(nb_players, matchs, fields)'.
        """
        nb_players = 4
        fields = 5
        matchs = [
            ['1', 'A', '2', 'B'], ['1', 'A', '2', 'C'],
            ['1', 'A', '2', 'D'], ['1', 'A', '3', 'B'],
            ['1', 'A', '3', 'C'], ['1', 'A', '3', 'D'],
            ['1', 'A', '4', 'B'], ['1', 'A', '4', 'C'],
            ['1', 'A', '4', 'D'], ['1', 'B', '2', 'A'],
            ['1', 'B', '2', 'C'], ['1', 'B', '2', 'D'],
            ['1', 'B', '3', 'A'], ['1', 'B', '3', 'C'],
            ['1', 'B', '3', 'D'], ['1', 'B', '4', 'A'],
            ['1', 'B', '4', 'C'], ['1', 'B', '4', 'D'],
            ['1', 'C', '2', 'A'], ['1', 'C', '2', 'B'],
            ['1', 'C', '2', 'D'], ['1', 'C', '3', 'A'],
            ['1', 'C', '3', 'B'], ['1', 'C', '3', 'D'],
            ['1', 'C', '4', 'A'], ['1', 'C', '4', 'B'],
            ['1', 'C', '4', 'D'], ['1', 'D', '2', 'A'],
            ['1', 'D', '2', 'B'], ['1', 'D', '2', 'C'],
            ['1', 'D', '3', 'A'], ['1', 'D', '3', 'B'],
            ['1', 'D', '3', 'C'], ['1', 'D', '4', 'A'],
            ['1', 'D', '4', 'B'], ['1', 'D', '4', 'C'],
            ['2', 'A', '3', 'B'], ['2', 'A', '3', 'C'],
            ['2', 'A', '3', 'D'], ['2', 'A', '4', 'B'],
            ['2', 'A', '4', 'C'], ['2', 'A', '4', 'D'],
            ['2', 'B', '3', 'A'], ['2', 'B', '3', 'C'],
            ['2', 'B', '3', 'D'], ['2', 'B', '4', 'A'],
            ['2', 'B', '4', 'C'], ['2', 'B', '4', 'D'],
            ['2', 'C', '3', 'A'], ['2', 'C', '3', 'B'],
            ['2', 'C', '3', 'D'], ['2', 'C', '4', 'A'],
            ['2', 'C', '4', 'B'], ['2', 'C', '4', 'D'],
            ['2', 'D', '3', 'A'], ['2', 'D', '3', 'B'],
            ['2', 'D', '3', 'C'], ['2', 'D', '4', 'A'],
            ['2', 'D', '4', 'B'], ['2', 'D', '4', 'C'],
            ['3', 'A', '4', 'B'], ['3', 'A', '4', 'C'],
            ['3', 'A', '4', 'D'], ['3', 'B', '4', 'A'],
            ['3', 'B', '4', 'C'], ['3', 'B', '4', 'D'],
            ['3', 'C', '4', 'A'], ['3', 'C', '4', 'B'],
            ['3', 'C', '4', 'D'], ['3', 'D', '4', 'A'],
            ['3', 'D', '4', 'B'], ['3', 'D', '4', 'C']
        ]
        result_ok1 = [['1', 'A', '2', 'B'], ['3', 'C', '4', 'D']]

        test_serie = calculate_one_serie(nb_players,
                                         matchs,
                                         fields)
        self.assertEqual(test_serie, result_ok1)
